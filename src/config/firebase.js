import firebase from 'firebase';

const firebaseConfig = {
  apiKey: 'AIzaSyCyCgJSoWQFi7OJdpBWqqZKeMpEQ7Gtqn0',
  authDomain: 'chousei-firebase-8d6ad.firebaseapp.com',
  databaseURL: 'https://chousei-firebase-8d6ad.firebaseio.com',
  projectId: 'chousei-firebase-8d6ad',
  storageBucket: '',
  messagingSenderId: '507760684066',
  appId: '1:507760684066:web:1daecad21ea8a837',
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
